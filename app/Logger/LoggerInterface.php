<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 09.02.2018
 * Time: 16:30
 */

namespace Logger;


interface LoggerInterface
{
    public function log(string $message): void;
}