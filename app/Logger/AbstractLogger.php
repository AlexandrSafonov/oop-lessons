<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 10.02.2018
 * Time: 12:49
 */

namespace Logger;


abstract class AbstractLogger
{
    protected function getDate(): string
    {
        return date("d-m-y");
    }
}