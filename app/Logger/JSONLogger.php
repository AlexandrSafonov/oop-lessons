<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 07.02.2018
 * Time: 11:28
 */

namespace Logger;


class JSONLogger extends AbstractLogger implements LoggerInterface
{
    public function log(string $message): void
    {

        $content = $this->getContent();

        $content[] = [
            'message' => $message,
            'date'    => $this->getDate(),
        ];

        $encodedContent = $this->encodeJSON($content);

        $this->saveInFile($encodedContent);
    }

    private function saveInFile(string $content): void
    {
        file_put_contents('logs/log.json', $content, LOCK_EX);
    }

    private function getContent(): array {
        $content = file_get_contents('logs/log.json');
        $decodedJSON = $this->decodeJSON($content);

        if (empty($content)) {
            $decodedJSON = array();
        }

        return $decodedJSON;
    }

    private function decodeJSON($content): array
    {
       return (array) json_decode($content);
    }

    private function encodeJSON($content): string
    {
        return json_encode($content);
    }
}