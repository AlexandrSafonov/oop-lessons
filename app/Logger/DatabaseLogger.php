<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 05.02.2018
 * Time: 19:40
 */

namespace Logger;

use PDO;


class DatabaseLogger extends AbstractLogger implements LoggerInterface
{
    private $dbh;

    private $serverName = '127.0.0.1';

    private $username = 'root';

    private $password = "";

    private $dataBase = 'sum_db';

    public function log(string $message): void
    {
        $transformedToArray = [
            'message' => $message,
            'date'    => $this->getDate(),
        ];

        $this->saveInDatabase($transformedToArray);
    }

    private function saveInDatabase(array $message)
    {
        $this->dbh = new PDO("mysql:host=$this->serverName;dbname=$this->dataBase;", $this->username, $this->password);

        $statement = $this->dbh->prepare("INSERT INTO sum_result(message, date) VALUES(:message, :date)");
        $statement->execute($message);
    }
}