<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 04.02.2018
 * Time: 11:47
 */

namespace Logger;

class TxtLogger extends AbstractLogger implements LoggerInterface
{

    public function log(string $message): void
    {
        $date = $this->getDate();
        $this->saveInFile($message, $date);
    }

    private function saveInFile(string $message, string $date): void
    {
        $string = $message . ' | ' . $date . "\n";
        file_put_contents('logs/log.txt', $string, FILE_APPEND | LOCK_EX);
    }


}