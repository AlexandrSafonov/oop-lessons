<?php

namespace Price;

use Logger\LoggerInterface;
use Money\Money;

class PriceCalculator
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getSum(array $prices)
    {
        $sum = 0;

        foreach ($prices as $price) {
            $sum += $price->getAmount();
        }

        $this->logger->log('Full price is: ' . $sum);

        return new Money(
            $sum,
            $this->getCurrency($prices)
        );
    }

    private function getCurrency($prices)
    {
        $price = $prices[0];

        return $price->getCurrency();
    }

}