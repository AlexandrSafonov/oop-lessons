<?php

require_once '../vendor/autoload.php';

use Money\Money;
use Money\Currency;
use Money\CurrencyConverter;

use Price\PriceCalculator;

use Logger\TxtLogger;
use Logger\JSONLogger;
use Logger\DatabaseLogger;

$dollar = new Currency(
     'USD',
     '$',
     1
);

$hrivna = new Currency(
    'HRN',
    'H',
    30.5
);

$condomPrice = new Money(
    2.002,
    $dollar
);

$bierBox = new Money(
    3.003,
    $dollar
);

$prices = [
    $condomPrice,
    $bierBox
];

$txtLogger = new TxtLogger();
$jsonLogger = new JSONLogger();
$databaseLogger = new DatabaseLogger();

$priceCalculatorWithTxtLogger = new PriceCalculator($txtLogger);
$priceCalculatorWithJSONLogger = new PriceCalculator($jsonLogger);
$priceCalculatorWithDatabaseLogger = new PriceCalculator($databaseLogger);

$currencyConverter = new CurrencyConverter();

$orderPrices = $priceCalculatorWithTxtLogger->getSum($prices);
$orderPrices = $priceCalculatorWithJSONLogger->getSum($prices);
$orderPrices = $priceCalculatorWithDatabaseLogger->getSum($prices);

$priceInHryvnia = $currencyConverter->convert($orderPrices, $hrivna);

print_r($priceInHryvnia);







