<?php

require_once '../vendor/autoload.php';

use Money\Money;
use Money\Currency;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

$container = new ContainerBuilder();
$loader = new YamlFileLoader($container, new FileLocator(__DIR__));
$loader->load('TaxCalculator\config\services.yml');

$dollar = new Currency(
    'USD',
    '$',
    1
);

$firstFamilyMemberSalary = new Money(
    50000,
    $dollar
);

$secondFamilyMemberSalary = new Money(
    60000,
    $dollar
);

$thirdTaxClassCalculator = $container->get('taxCalculator.thirdClass');
$fourthTaxClassCalculator = $container->get('taxCalculator.fourthClass');

$tax = $thirdTaxClassCalculator->getTax($firstFamilyMemberSalary, $secondFamilyMemberSalary);
$tax = $fourthTaxClassCalculator->getTax($firstFamilyMemberSalary, $secondFamilyMemberSalary);

foreach ($tax as $familyMember) {
    print_r('For second salary tax is: '
        . $familyMember->getAmount()
        . $familyMember->getCurrency()->getSymbol() . "\n");
}