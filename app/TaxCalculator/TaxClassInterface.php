<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.02.2018
 * Time: 15:58
 */

namespace TaxCalculator;

use Money\Money;

interface TaxClassInterface
{
    public function deductNontaxableMoney(Money $firstSalary, Money $secondSalary): array;
}