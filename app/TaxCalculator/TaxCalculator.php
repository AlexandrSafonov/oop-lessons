<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.02.2018
 * Time: 15:56
 */

namespace TaxCalculator;

use Money\Money;

class TaxCalculator implements TaxCalculatorInterface
{
    private $taxClass;

    private $taxRate = 40;

    public function __construct(TaxClassInterface $taxClass)
    {
        $this->taxClass = $taxClass;
    }

    public function getTax(Money $firstSalary, Money $secondSalary): array
    {
        $salaryOnClass = $this->taxClass->deductNontaxableMoney($firstSalary, $secondSalary);

        $firstFamilyMemberSalaryTax = new Money(
            $this->calculateTax($salaryOnClass['firstSalary']),
            $firstSalary->getCurrency()
        );

        $secondFamilyMemberSalaryTax = new Money(
            $this->calculateTax($salaryOnClass['secondSalary']),
            $firstSalary->getCurrency()
        );

        return [
            'firstFamilyMemberSalaryTax' => $firstFamilyMemberSalaryTax,
            'secondFamilyMemberSalaryTax' => $secondFamilyMemberSalaryTax,
        ];
    }

    private function calculateTax(int $salary): int
    {
        return $salary / 100 * $this->taxRate;
    }

}