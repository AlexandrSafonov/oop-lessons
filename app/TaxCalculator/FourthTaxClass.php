<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.02.2018
 * Time: 16:00
 */

namespace TaxCalculator;

use Money\Money;

class fourthTaxClass implements TaxClassInterface
{
    public function deductNontaxableMoney(Money $firstSalary, Money $secondSalary): array
    {
        return [
            'firstSalary'  => $firstSalary->getAmount() - 14000,
            'secondSalary' => $secondSalary->getAmount(),
        ];
    }
}