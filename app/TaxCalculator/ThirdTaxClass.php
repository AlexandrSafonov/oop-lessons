<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.02.2018
 * Time: 15:59
 */

namespace TaxCalculator;

use Money\Money;


class thirdTaxClass implements TaxClassInterface
{
    public function deductNontaxableMoney(Money $firstSalary, Money $secondSalary): array
    {
        return [
            'firstSalary'  => $firstSalary->getAmount() - 6000,
            'secondSalary' => $secondSalary->getAmount() - 6000,
        ];
    }
}