<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 14.02.2018
 * Time: 17:12
 */

namespace TaxCalculator;

use Money\Money;

interface TaxCalculatorInterface
{
    public function getTax(Money $firstSalary, Money $secondSalary): array;
}