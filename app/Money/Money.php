<?php
/**
 * Created by PhpStorm.
 * User: Marina
 * Date: 03.02.2018
 * Time: 13:38
 */

namespace Money;

class Money
{
    private $amount;

    private $currency;

    public function __construct(
        float       $amount,
        Currency    $currency
    ) {
        $this->amount = $amount;
        $this->currency = $currency;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function getCurrency()
    {
        return $this->currency;
    }
}