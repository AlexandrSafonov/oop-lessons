<?php

namespace Money;

class Currency
{
    private $code;

    private $symbol;

    private $course;

    public function __construct(
        string $code,
        string $symbol,
        float $course
    ) {
        $this->code     = $code;
        $this->symbol   = $symbol;
        $this->course   = $course;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getSymbol()
    {
        return $this->symbol;
    }

    public function getCourse()
    {
        return $this->course;
    }
}