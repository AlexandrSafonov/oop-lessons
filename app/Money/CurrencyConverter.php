<?php

namespace Money;

class CurrencyConverter
{
    public function convert(
        Money $price,
        Currency $currency
    ) {
        $course = $currency->getCourse();

        $amountInDollar = $this->getAmountInDollar($price);
        $convertedAmount = $amountInDollar * $course;

        return new Money(
            $this->formatPrice($convertedAmount),
            $currency
        );

    }

    private function getAmountInDollar(Money $price)
    {
        return $price->getAmount() / $price->getCurrency()->getCourse();
    }

    private function formatPrice($price)
    {
        return round($price, 2);
    }
}